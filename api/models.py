from django.db import models

from django.db import models

class Adults(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    age = models.IntegerField(blank=True, null=True)
    workclass = models.CharField(max_length=50, blank=True)
    fnlwgt = models.IntegerField(blank=True, null=True)
    education = models.CharField(max_length=50, blank=True)
    educationnum = models.IntegerField(db_column='educationNum', blank=True, null=True)  # Field name made lowercase.
    marital_status = models.CharField(max_length=50, blank=True)
    occupation = models.CharField(max_length=50, blank=True)
    relationship = models.CharField(max_length=50, blank=True)
    race = models.CharField(max_length=50, blank=True)
    sex = models.CharField(max_length=50, blank=True)
    cgain = models.IntegerField(blank=True, null=True)
    closs = models.IntegerField(blank=True, null=True)
    hpo = models.IntegerField(blank=True, null=True)
    country = models.CharField(max_length=50, blank=True)
    income = models.CharField(max_length=50, blank=True)

    class Meta:
        managed = False
        db_table = 'adults'